#!/usr/bin/env python

# Light each LED in sequence, and repeat.

import opc
import time

LEDS = 60
client = opc.Client('localhost:7890')

while True:
    for i in range(LEDS):
        pixels = [(0,0,0)] * LEDS
        pixels[i] = (255,255,255)
        client.put_pixels(pixels)
        time.sleep(0.01)
