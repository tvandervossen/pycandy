#!/usr/bin/env python
# coding=utf-8

import opc
import pyaudio
import struct
import math
import cmath
import os
import time
import colorsys
import argparse

parser = argparse.ArgumentParser(description = 'LED string audio spectrum visualizer for Fadecandy.')
parser.add_argument('-i', '--input', help = 'audio input to use (defaults to 0)', default = 0, type = int,  metavar = 'id')
parser.add_argument('-l', '--list', help = 'list available audio inputs and their ids', action = 'store_true')
parser.add_argument('-d', '--debug', help = 'show debugging output', action = 'store_true')
args = parser.parse_args()

pa = pyaudio.PyAudio()

if (args.list):
    for i in range(pa.get_host_api_info_by_index(0).get('deviceCount')):
        if (pa.get_device_info_by_host_api_device_index(0, i).get('maxInputChannels')) > 0:
            print("%d: %s" % (i, pa.get_device_info_by_host_api_device_index(0, i).get('name')))
    exit(0)

LEDS = 60
HUE_STEP = -1.0 / LEDS * 1.9

FORMAT = pyaudio.paInt16
CHANNELS = 1
RATE = 44100 / 8
FRAMES_PER_BLOCK = LEDS * 4

SHORT_NORMALIZE = 1.0 / 32768.0

# Converts a string of 16-bit samples packed as byte pairs into a list of values between -1 and 1.
def block_to_floats(block):
    shorts = struct.unpack("%dh" % (len(block) / 2), block)
    return [i * SHORT_NORMALIZE for i in shorts]

# Given a list of short integers, return the root mean square normalized as a value between 0 and 1
def rms(values):
    sum = 0.0
    for value in values:
        sum += value**2
    return math.sqrt(sum / len(values))

# Compute the discrete Fourier transform using complex number
def dft_complex(input):
    n = len(input)
    output = []
    for k in range(n):  # For each output element
        s = complex(0)
        for t in range(n):  # For each input element
            angle = 2j * cmath.pi * t * k / n
            s += input[t] * cmath.exp(-angle)
        output.append(s)
    return output

# Convenience function to get amplitudes for each frequency component
def dft_amplitudes(input):
    result = dft_complex(input)
    output = []
    # Bands are mirrored in the result of the Fourier transform, so we only use the first half
    for i in range(len(result) / 2):
        # The amplitude of each band is obtained by taking the absolute of the complex number
        output.append(abs(result[i]))
    return output

opc = opc.Client('localhost:7890')

bands = [0.0] * LEDS
pixels = [(0,0,0)] * LEDS

stream = pa.open(format=FORMAT, channels=CHANNELS, rate=RATE, input=True, frames_per_buffer=FRAMES_PER_BLOCK, input_device_index=args.input)

errorcount = 0

while True:
    floats = block_to_floats(stream.read(FRAMES_PER_BLOCK, exception_on_overflow=False))

    amplitudes = dft_amplitudes(floats)

    if (args.debug):
        os.system('clear')

    for i in range(LEDS / 2):
        amplitude = (
            amplitudes[i * 2 + 2] +
            amplitudes[i * 2 + 3] +
            amplitudes[i * 2 + 4]
            ) * 0.01

        if amplitude > 0.01 and amplitude > bands[i]:
            bands[i] = min(amplitude, 1)
        else:
            bands[i] = bands[i] * 0.75

        color = tuple(int(round(i * 255)) for i in colorsys.hsv_to_rgb(i * HUE_STEP, 1 - bands[i] * 0.65, 0.1 + bands[i] * 0.9))

        pixels[i] = color
        pixels[LEDS - 1 - i] = color

        if (args.debug):
            print("%2d %2d %.2f %+.2f " % (i, LEDS - 1 - i, i * HUE_STEP, bands[i]) + '▇' * int(round(bands[i] * 32)))

    opc.put_pixels(pixels)
