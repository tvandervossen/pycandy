# PyCandy

A LED string audio spectrum visualizer for Fadecandy.

## Installation on OS X

    $ brew install portaudio 
    $ pip install pyaudio

## Usage

Open a Terminal and start the Fadecandy server:

    $ ./fcserver

You should get a message like `USB device Fadecandy (…) attached.` if the server could find the connected Fadecandy device.

Now open a new Terminal tab and start the visualizer script:

    $ ./candy.py

In most cases, this will use the microphone input by default. You can list the available inputs with:

    $ ./candy.py --list
    0: MacBook Pro Microphone
    2: Loopback Audio

And use a different input device with the optional `--input` argument:

    $ ./candy.py --input 2

You can also enable debugging output:

    $ ./candy.py --debug
